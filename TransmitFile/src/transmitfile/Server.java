package transmitfile;


import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {
    ServerSocket serversocket;
    private Socket sock;

    public static void main(String[] args) {
        Server server = new Server();
        server.run();
    }
    
    public void run()
    {
        try
        {
            serversocket = new ServerSocket(5432);
            sock = serversocket.accept();
            recieveFile();
        }
        catch(IOException e){}
    }
    
    
    private void recieveFile()
    {
        // путь куда будем сохранять
        String path = "C:\\Users\\Dia\\Desktop\\im.png";
        File file = new File(path);
        try
        {
            DataInputStream in = new DataInputStream(sock.getInputStream());
            long s;
            // считали размер файла
            s = in.readLong();
           
            System.out.println("File size: " + s);
            
            
            byte[] byteArray = new byte[1024];
            File f = new File(path);
            f.createNewFile();
            FileOutputStream fos = new FileOutputStream(f);
            int sp = (int)(s / 1024);
            if (s % 1024 != 0) sp++;
            
            BufferedInputStream bis = new BufferedInputStream(sock.getInputStream());
            while(s > 0)
            {
                int i = bis.read(byteArray);
                fos.write(byteArray, 0, i);
                s-= i;
            }
            fos.close();
            
        }
        catch (IOException e){}
    }
}
