package transmitfile;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {
    // поля
    private Socket socket;
    String path;
    
    public static void main(String[] args)
    {
        Client client = new Client();
        client.startClient();
    }
    
    public void startClient()
    {
        path = "C:\\Users\\Dia\\Desktop\\";
        File file = new File(path, "1.png");
        
        try
        {
            // коннектимся
            socket = new Socket("127.0.0.1", 5432);
            System.out.println("connected");
            sendFile(file);
            
        }
        catch(Exception e){}
        
    }
    
public void sendFile(File file)
    {
        try
        {
            // передаем серверу размер файла
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            long s = file.length();
            out.writeLong(s);
           
            System.out.println("File size: " + s + " name: " + file.getPath());
            
            // массив куда мы буде пихать байты
            byte[] byteArray = new byte[1024];
            
            // передаем сам файл
            FileInputStream fis = new FileInputStream(file.getPath());
            int sp = (int)(s / 1024);
            if (s % 1024 != 0) sp++;
            
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            while(s > 0)
            {
                int i = fis.read(byteArray);
                bos.write(byteArray, 0, i);
                s-= i;
                //System.out.println("s = " + s);
            }
            System.out.println("send");
            bos.flush();
            fis.close();
            
        }
        catch (IOException e){}
    }
}
