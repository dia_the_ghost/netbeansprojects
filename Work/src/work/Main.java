package work;

import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Cistern cistern = new Cistern(20);          // вес цистерны = 10 кг
        Container container = new Container(30);    // вес контейнера = 15 кг
        Platform platform = new Platform(15);
        Human human = new Human(70);
        
        // создаем массив всех объектов 
        Parom parom = new Parom();
        parom.transport(cistern.weight, cistern);
        parom.transport(container.weight, container);
        parom.transport(platform.weight, platform);
        parom.transport(human.weight, human);
        
        for(ITrippable temp : parom.m)
        {
            System.out.println(temp.toString());
        }
        
        System.out.println("");
        
        if(parom.canTransport())
        {
            System.out.println("Вес в норме, можно транспортировать!");
        }
        else
        {
            System.out.println("Вес груза превышает допустимое значение!");
        }
        // достаем массив 
        List<ITrippable> massiv = parom.m;
        
        // оба объекта должны быть ITrippable
        Collections.sort(massiv, ITrippable.ITrippableWeight);
        
        for(ITrippable temp : parom.m)
        {
            System.out.println(temp.toString());
        }
    }
}
