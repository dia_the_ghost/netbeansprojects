package work;

import java.util.Comparator;

public interface ITrippable { // ЭТО ИНТЕРФЕЙС И В НЕМ КОМПАРАТОР

    public int countWeight(int w);
    public int getWeight();
    
    public static Comparator<ITrippable> ITrippableWeight = (ITrippable t, ITrippable t1) -> {
        
        int a = t.getWeight();
        int b = t1.getWeight();
        
        return a - b;
    };
}
