package work;

public class Cistern extends Gruz {

    private int cisternMass = 10;

    public Cistern(int w) {
        this.weight = w;
        this.weight = this.countWeight(w);
    }

    @Override
    public int countWeight(int w) {
        return this.weight = cisternMass + this.weight;
    }
    
    @Override
    public String toString()
    {
        String s = "Вес цистерны с грузом: " + Integer.toString(this.weight) + " килограмм ";
        return s;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }
}
