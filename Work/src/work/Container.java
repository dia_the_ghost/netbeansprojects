package work;

public class Container extends Gruz {

    private int containerMass = 15;

    public Container(int w) {
        this.weight = w;
        this.weight = this.countWeight(w);
    }

    @Override
    public int countWeight(int w) {
        return this.weight = containerMass + this.weight;
    }
    
    @Override
    public String toString()
    {
        String s = "Вес контейнера с грузом: " + Integer.toString(this.weight) + " килограмм ";
        return s;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }
}
