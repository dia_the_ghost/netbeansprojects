package work;

public class Human implements ITrippable {

    public int weight;

    public Human(int w) {
        this.weight = w;
        this.weight = this.countWeight(w);
    }

    @Override
    public int countWeight(int w) {
        return this.weight;
    }
    
    @Override
    public String toString()
    {
        String s = "Вес человека: " + Integer.toString(this.weight) + " килограмм ";
        return s;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }
}
