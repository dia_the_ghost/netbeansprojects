package work;

public class Platform extends Gruz implements ITrippable {

    private int platformMass = 0;

    public Platform(int w) {
        this.weight = w;
        this.weight = this.countWeight(w);
    }

    @Override
    public int countWeight(int w) {

        return this.weight = platformMass + this.weight;
    }
    
    @Override
    public String toString()
    {
        String s = "Вес объекта на платформе: " + Integer.toString(this.weight) + " килограмм ";
        return s;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }
}
