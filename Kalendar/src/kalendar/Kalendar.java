
package kalendar;

import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class Kalendar {
    static Date d;
    static DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
    static String date;
    static Calendar cal;
    static GregorianCalendar gc;

    public static void main(String[] args) {
        // ждем ввода пользоватиля
        Scanner sc = new Scanner(System.in);
        String year = sc.next();
        
        // делаем из строки чесло
        int n_year = Integer.parseInt(year);
        
        // скармливаем год методу в зависимости от года
        if(n_year < 1918)
        {
            countBefore1918(n_year);
        }
        else if(n_year == 1918)
        {
            countIn1918(n_year);
        }
        else if(n_year > 1918)
        {
            countAfter1918(n_year);
        }
    }
    
    
    public static void countBefore1918(int year) // юлианский календарь. я его не нашла поетому юзаю обычный
    {
        cal = Calendar.getInstance();  // вместо сегодняшнего дня сунуть дату
        cal.set(year, 00, 01);         // суем нужную дату
        cal.add(Calendar.DAY_OF_MONTH, 255 - 14); // прибавляем дни
        d = cal.getTime();
        
        date = df.format(d);
        System.out.println(date);
        
        System.out.println(cal.getCalendarType()); // говорит что грегорианский а надо юлианский
    }
    
    public static void countIn1918(int year)
    {
        cal = Calendar.getInstance();  // вместо сегодняшнего дня сунуть дату
        cal.set(1918, 00, 01);
        cal.add(Calendar.DAY_OF_MONTH, 241);
        d = cal.getTime();
        
        date = df.format(d);
        System.out.println(date);
    }
    
    public static void countAfter1918(int year) // грегорианский календарь  GregorianCalendar
    {
        gc = new GregorianCalendar();
        gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
        gc.set(GregorianCalendar.MONTH, GregorianCalendar.JANUARY);
        gc.set(GregorianCalendar.YEAR, year);
        // прибавляем дни
        gc.add(GregorianCalendar.DAY_OF_MONTH, 255);
        d = gc.getTime();
        
        date = df.format(d);
        System.out.println(date);
        
        System.out.println(gc.getCalendarType());
    }
}
