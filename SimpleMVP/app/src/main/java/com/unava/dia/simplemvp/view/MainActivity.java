package com.unava.dia.simplemvp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.unava.dia.simplemvp.R;
import com.unava.dia.simplemvp.model.Data;
import com.unava.dia.simplemvp.presenter.Presenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private Presenter presenter;
    private Data model;

    @BindView(R.id.textView) public TextView textView;
    @BindView(R.id.editText) EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // настраиваем Presenter, даем ему нашу модель и view
        init();

        // вешаем лиснер
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            // сообщаем презентеру что кнопка смені текста біла нажата
            // и он сам уже решит что нужно выставить в textView текст из data
            @Override
            public void onClick(View view) {
                presenter.changeSign();
            }
        });
    }
    private void init()
    {
        model = new Data();
        presenter = new Presenter(model);
        presenter.attachView(this);
    }

    // эту функцию ВЫЗЫВАЕТ Presenter в changeSign()
    public Data getData() {
        model = new Data();
        model.setString(editText.getText().toString());
        return model;
    }
}
