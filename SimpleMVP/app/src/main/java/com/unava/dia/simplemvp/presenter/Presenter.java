package com.unava.dia.simplemvp.presenter;

import com.unava.dia.simplemvp.model.Data;
import com.unava.dia.simplemvp.view.MainActivity;

/**
 * Created by Dia on 19.07.2018.
 */

public class Presenter {
    MainActivity view;
    Data model;

    public Presenter(Data model)
    {
        this.model = model;
    }
    public void attachView(MainActivity view)
    {
        this.view = view;
    }

    public void detachView()
    {
        view = null;
    }


    // нам сказали, что біла нажата кнопка
    // логика презентера считает, что мы должны вызвать гет дата и припихать их в textView
    // у  MainActivity view
    public void changeSign()
    {
        Data d = view.getData();
        view.textView.setText(d.getString());
    }
}
