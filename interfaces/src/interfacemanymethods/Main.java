package interfacemanymethods;


interface Interface1 {
    // в интерфейсах не создается конструктор по умолчанию
    void method();
}

interface Interface2 {
    void method();
}
class ConcretClass implements Interface1, Interface2
{
    public void method()
    {
        System.out.println("method 1-2");
    }
}

public class Main {
    public static void main(String[] args)
    {
        ConcretClass instance = new ConcretClass();
        instance.method();
    }
}
