package interfinherit;

public class Main {
    public static void main(String[] args)
    {
        ConcreteClass instance = new ConcreteClass();
        instance.method1();
        instance.method2();
    }
}
