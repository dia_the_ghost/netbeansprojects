
package methodandclass;

interface Interface {
    // в интерфейсах не создается конструктор по умолчанию
    void method();
}

class BaseClass
{
    public void method()
    {
        System.out.println("method in base class");
    }
}
class DerivedClass extends BaseClass implements Interface
{
    // реализация интерфейса необязательна тк 
    // сигнатуры методов в базовом классе и интерфейсе совпадают
}

public class Main {
    public static void main(String[] args)
    {
        DerivedClass instance = new DerivedClass();
        instance.method();
        
        // UpCast
        Interface instance1 = (Interface)instance;
        instance1.method();
    }
}
