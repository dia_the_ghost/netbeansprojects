package inter;


public interface ITest extends I1, I2{
    public static final int PRICE = 12;
    public abstract void method();
    
    static int sum()
    {
        return 5 + 12;
    }
    
    default int mul()
    {
        return 10 * 5;
    }
}
