/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Dia
 */
public class Interfaces {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Interface1 instance1 = new DerivedClass();
        Interface2 instance2 = new DerivedClass();
        
        DerivedClass instance3 = new DerivedClass();
        
        instance1.method1();
        instance2.method2();
        
        instance3.method1();
        instance3.method2();
    }
    
}
