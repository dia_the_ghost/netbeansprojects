package arrays;
import java.util.ArrayList;
import java.util.Arrays;


public class Main {

    public static void main(String[] args) {
        // зубчатый массив
        int [][] arr =
        {
            {10, 15},
            {3, 2, 1, 0},
            {65, 12, 42},
            {}
        };
        
        System.out.println(arr.length);
        
        int[] a = {10, 2, 3, 6, 12};
        int[] a1 = {10, 2, 3, 6, 12};
        Arrays.sort(a);
        Arrays.sort(a1);
            
        System.out.println(a);                          // отобразит место в памяти
        System.out.println(Arrays.toString(a));         // выведет сам массив
        System.out.println(Arrays.binarySearch(a, 12)); // поиск элемента в массиве
        System.out.println(Arrays.equals(a, a1));       // сравнивает массивы
        
        
        int[] a2 = new int[5];
        
        Arrays.fill(a2, 10);
        System.out.println(Arrays.toString(a2)); 
        
        
        Arrays.copyOf(a2, 10);
        Arrays.fill(a2, 11);
        System.out.println(Arrays.toString(a2)); 
        
        ArrayList<Integer> li = new ArrayList<>();
        li.add(666);
        System.out.println(li.get(0)); 
    }
}
