package arraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> li = new ArrayList<>();
        
        li.add("0");
        li.add("1");
        li.add("2");
        
        System.out.println(li);
        
        li.add(0, "f");
        li.remove(1);  
        li.remove("2");
        
        System.out.println(li);
        
        ArrayList<Integer> s = new ArrayList<>();
        
        s.add(2);
        s.add(2);
        s.add(0b1000);
        
        Iterator<Integer> it = s.iterator();
        while(it.hasNext())
        {
            Integer a = it.next();
            System.out.println(a);
        }
        
        ListIterator<Integer> listIterator = s.listIterator();
        while(listIterator.hasNext())
        {
            Integer a = listIterator.next();
            System.out.println(a);
        }
        
        for(int i = 0; i < 15; i++)
        {
            s.add(i);
        }
        System.out.println("---------------------------------------------");
        for(Integer temp : s)
        {
            System.out.println(temp);
        }
        System.out.println("---------------------------------------------");
    }
}
