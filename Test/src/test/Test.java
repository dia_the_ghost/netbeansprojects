package test;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        Number [] num  = new Integer[10];
        num[0] = 10.10d;
        
        System.out.println(num[0]);
        
        //в джаве массивы ковариантны (элементы одного типа)
        //дженерики могут быть разного типа
        // дженерики инвариантны
        //List<Number> list = new ArrayList<Integer>();
    }
    
}
