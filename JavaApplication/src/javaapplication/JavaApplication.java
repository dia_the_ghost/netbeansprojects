/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication;

import static java.lang.Math.pow;
import java.util.Scanner;

public class JavaApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
    //boolean - булев тип, может иметь значения true или false
    //byte - 8-разрядное целое число
    //short - 16-разрядное целое число
    //int - 32-разрядное целое число
    //long - 64-разрядное целое число
    //char - 16-разрядное беззнаковое целое, представляющее собой символ UTF-16 (буквы и цифры)
    //float - 32-разрядное число в формате IEEE 754 с плавающей точкой
    //double - 64-разрядное число в формате IEEE 754 с плавающей точкой


        // от +127 до -128
        byte a = 127;
        byte b = -128;
        
        
        // от +32767 до -32768
        short c = 32767;
        short d = -32767;
        
        // int - от +2147483647 до -2147483648 в 32 битах
        int e = 2147483647;
        int f = -2147483648;
        
        long g = -9223372036854775808l;
        long h = 9223372036854775807l;
        
       
        // точность в 8 символов
        float i = 3.4e+38f;
        float j = -1.4e-45f;
        
        // от +1.7e+308 до -4.9e-324 
        double k = -4.9e-324;
        double l = 1.7e+308;
        
        boolean flag = true;
        
        
        char ch = 'A';
        char ca = 0x0041;
        char cb = '\u265e';
        
        String str = "Hello, world!";
        
        Integer sa = 7;
        Double sb = 232.0;
        
        //System.out.println(ch);
        //System.out.println(ca);
        //System.out.println(cb);
        
        final int m = 24;
        byte n = 0;
        n = (byte) m;
        
        //System.out.println(m);
        
        // 2 в степени 8
        double result = pow(2, 8);
        
        result = Math.sqrt(256);
        result = Math.random();
        
        System.out.println(result);
        
        System.out.printf("Это число %2$s и %1$s", 1, 5);
        
        int varInt = 7;
        float varFloat = 7f;
        System.out.println(varInt == varFloat);
        
        // Input (Scanner)
        Scanner input = new Scanner(System.in);
        
        int inp = input.nextInt();
        float inpf = input.nextFloat();
        String inpc = input.next();
        
        
        
        System.out.println(inp + "\n" + inpf + "\n" + inpc + "\n");
        
    }
    
}
