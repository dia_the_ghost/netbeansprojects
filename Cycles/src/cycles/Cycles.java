package cycles;

import java.util.Scanner;

public class Cycles {

    public static void main(String[] args) {
        int counter = 4;
        int factorial = 1;
        
        System.out.println("Факториал числа: " + counter + " ! = ");
        
        do
        {
            factorial *= counter--;
            
        } while(counter > 0);
        
        System.out.print(factorial);
        
        Scanner in = new Scanner(System.in);
        
        char character;
        
        for( ; ; )
        {
            character = in.next().charAt(0);
            
            switch(character)
            {
                case 'l':
                    System.out.println("Go left! ");
                    continue;
                    
                case 'r':
                    System.out.println("Go right! ");
                    continue;                    
            }
            
            switch(character)
            {
                case 'w':
                    System.out.println("Go top! ");
                    continue;
                    
                case 's':
                    System.out.println("Go bottom! ");
                    continue;   
                    
                default:
                {
                    System.out.println("Exit! ");
                    break;
                }
            }
            
            break;
        }
   }
}
