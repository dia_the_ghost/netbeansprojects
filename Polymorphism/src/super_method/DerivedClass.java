
package super_method;

public class DerivedClass extends BaseClass{
    @Override
    public void method()
    {
        super.method();
        System.out.println("Method from Derived Class");
    }
}
