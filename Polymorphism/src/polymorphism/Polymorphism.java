
package polymorphism;

public class Polymorphism {

    public static void main(String[] args) {
        DerivedClass instance = new DerivedClass();
        
        instance.field1 = 1;
        instance.field2 = 2;
        instance.field3 = 3;
        instance.field4 = 4;
        
        // UpCast
        BaseClass newInstance = (BaseClass) instance;
        
        System.out.println("field1 " + newInstance.field1);
        System.out.println("field2 " + newInstance.field2);
        //System.out.println("field3 " + newInstance.field3);
        //System.out.println("field4 " + newInstance.field4);
        
        System.out.println("Hash code for instance: " + instance.hashCode());
        System.out.println("Hash code for newInstance: " + newInstance.hashCode());
    }
    
}
