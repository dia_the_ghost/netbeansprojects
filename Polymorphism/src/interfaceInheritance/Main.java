
package interfaceInheritance;

/**
 *
 * @author Dia
 */
public class Main {
    public static void main(String[] args) {
        SomeClass a = new SomeClass();
        a.method();
        
        // UpCast
        SomeInterface aUp = a;
        aUp.method();
        
        // DownCast
        SomeClass aDown = (SomeClass)aUp;
        aDown.method();
        
        
    }   
}
