package interfaceInheritance;

public class SomeClass implements SomeInterface {
    
    public String publicField = "public field";
    
    public void method()
    {
        System.out.println("method.someClass");
    }

}
