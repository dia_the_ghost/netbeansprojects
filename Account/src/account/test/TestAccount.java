/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account.test;

import account.domain.Account;

/**
 *
 * @author Dia
 */
public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account(-255.666);
        myAccount.deposit(120);
        myAccount.withdraw(120);
        myAccount.withdraw(400);
        System.out.println(myAccount.getBalance());
    }
}
