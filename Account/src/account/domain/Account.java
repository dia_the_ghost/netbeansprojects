/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account.domain;

/**
 *
 * @author Dia
 */
public class Account {

    private double balance;

    /**
     * Пресваивает начальное значение баланса
     * @param balance balance must be positive
     */
    public Account(double balance) {
        if (balance >= 0) {
            this.balance = balance;
        } else {
            this.balance = 0;
        }
    }

    public double getBalance() {
        return balance;
    }

    /**
     * method for deposit money to account
     * @param amt 
     */
    public void deposit(double amt) {
        if (amt > 0) {
            balance += amt;
        }
    }

    /**
     * method to withdraw money
     * @param amt a positive amount of money
     */
    public void withdraw(double amt) {
        if (amt <= balance) {
            balance -= amt;
        }
    }

}
