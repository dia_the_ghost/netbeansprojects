package mygenerics;

public class MyGenerics<TYPE1, TYPE2> {
    
    public TYPE1 variable1;
    public TYPE2 variable2;
    
    public MyGenerics(TYPE1 argument1, TYPE2 argument2)
    {
        this.variable1 = argument1;
        this.variable2 = argument2;
    }
    
}
