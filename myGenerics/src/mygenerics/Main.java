package mygenerics;

public class Main {

    public static void main(String[] args) {
        MyGenerics<Integer, Integer> instance1 = new MyGenerics<>(1, 2);
        System.out.println(instance1.variable1 + instance1.variable2);
        
        MyGenerics<String, Integer> instance2 = new MyGenerics<>("Number", 2);
        System.out.println(instance2.variable1 + instance2.variable2);
        
        MyGenerics<String, String> instance3 = new MyGenerics<>("Hello, ", "World! ");
        System.out.println(instance3.variable1 + instance3.variable2);
    }
    
}
