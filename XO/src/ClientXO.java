import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

// TODO make gui
public class ClientXO extends JFrame {
    ButtonsPanel panel;
    BufferedReader readerToClientFromServer;
    public PrintWriter fromClientToServer;
    String str = "";
    public static String result = "";

    Socket socket = null;

    // ctor
    public ClientXO(){
        this.setTitle("TicTacToe");

        GridBagLayout mainLayout = new GridBagLayout();
        this.setLayout(mainLayout);

        try{
            socket = new Socket("127.0.0.1", 1234);
            readerToClientFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            //TODO add writer
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // поток для чтения
        new Thread(new Reciever()).start();

        panel = new ButtonsPanel();

        this.add(panel);
        pack();
    }
    public static void main(String[] args) {

        // нам нужна сетка с кнопками
        // и мы ее короче тут отобразим
        // и запустим поток коннектищийся к серверу

        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                ClientXO client = new ClientXO();
                client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                client.setLocationRelativeTo(null); // центрировать окно
                client.setVisible(true);
            }
        });
    }

    public void Send ()
    {
        // get socket
        try{
            // тут он ему дает
            // а должен давать не тут
            fromClientToServer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fromClientToServer.println(result);
    }

    private class Reciever implements Runnable {
        @Override
        public void run()
        {
            // принимаем то что дал сервер и парсим
            while (!socket.isClosed())
            {
                try{
                    while ((str = readerToClientFromServer.readLine()) != null)
                    {
                        System.out.println(str);
                        String [] params = str.split(":");
                        int row = Integer.parseInt(params[0]);
                        int column = Integer.parseInt(params[1]);

                        if(panel != null) panel.paintButton(row, column);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
