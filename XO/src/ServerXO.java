import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ServerXO implements Runnable{
    Socket clientSocket = null;
    ServerSocket ss = null;

    private Thread serverThread;

    BufferedReader  readerToClientFromServer = null;
    BufferedReader readerToServerFromClient = null;
    PrintWriter writerFromServerToClient = null;
    int [][] board;

    static boolean serverCanGo = true;
    static boolean clientCanGo = false;

    BlockingQueue<SocketProcessor> q = new LinkedBlockingQueue<>();

    //ctor
    public ServerXO(int port)
    {
        super();

        // хранит значения
        board = new int[3][3];

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {

                board[i][j] = 0;
            }
        }

        // стартуем сервер
        InetAddress serverIp = null;

        try
        {
            serverIp = InetAddress.getLocalHost();
            ss = new ServerSocket(port);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new ServerXO(1234).run();
    }

    // ожидает новое подключение
    private Socket getNewConnection()
    {
        Socket s = null;
        try {
            s = ss.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void run() {
        serverThread = Thread.currentThread();
        while (true)
        {
            Socket d = getNewConnection();
            if(serverThread.isInterrupted()){
                break;
            }
            else if(d != null){
                try{
                    final SocketProcessor processor = new SocketProcessor(d);
                    final Thread thread = new Thread(processor);
                    thread.setDaemon(true);
                    thread.start();
                    q.offer(processor);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class SocketProcessor implements Runnable {
        Socket s;
        BufferedReader br;
        PrintWriter pw;

        //ctor
        SocketProcessor(Socket socketParam) throws IOException
        {
            s = socketParam;
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            pw = new PrintWriter(s.getOutputStream(), true);
        }

        @Override
        public void run() {
            System.out.println("RUNNING!");
            while (!s.isClosed())
            {
                // считываем то что нам кинул клиент и парсим на 2 инта
                String line;
                try{
                    line = br.readLine();
                    String [] coords = line.split(":");
                    int row = Integer.parseInt(coords[0]);
                    int column = Integer.parseInt(coords[1]);


                    // TODO проверяем есть ли такой ход и если нету то добавляем
                    // TODO кидаем ход клиентам
                    String outputLine = String.valueOf(row);
                    outputLine = outputLine.concat(":");
                    outputLine = outputLine.concat(String.valueOf(column));

                    pw.println(outputLine);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
