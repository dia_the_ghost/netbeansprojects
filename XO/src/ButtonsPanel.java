import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

public class ButtonsPanel extends JPanel {
    GridLayout gl;

    // тут храним указатели на кнопки
    ArrayList<MyButton> li = new ArrayList();
    MyButton[][] btnarr;

    // стартовые значения для создания и удаления поля
    int nRows = 3;
    int nCols = 3;


    // ctor
    public ButtonsPanel()
    {
        gl = new GridLayout(nRows, nCols);
        createField(nRows, nCols);
    }

    public void createField(int rows, int columns)
    {
        gl.setRows(rows);
        gl.setColumns(columns);


        this.setLayout(gl);
        this.btnarr = new MyButton[rows][columns];

        // добавление кнопок в цикле
        for(int i = 0; i < rows; ++i)
        {
            for(int j = 0; j < columns; ++j)
            {
                addButton(mouseListener, i, j);
            }
        }

        this.updateUI();
        this.repaint();
    }

    private void addButton(MouseListener l, int i, int j)
    {
        MyButton b = new MyButton(i, j);
        b.addMouseListener(l);
        b.setPreferredSize(new Dimension(60, 60));
        // добавляем в список
        li.add(b);
        btnarr[i][j] = b;

        this.add(b);
    }

    MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent me) {
            MyButton temp = (MyButton)me.getSource();

            int row = 0;
            int column = 0;
            // ищем координаты кнопки
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    if(temp.equals(btnarr[i][j])) {
                        row = i;
                        column = j;
                    }
                }
            }

            // отправляем координаты кликнутой кнопки на сервер
            // через функцию Send()
            String s = "";
            s.concat(String.valueOf(column));
            s.concat(":");
            s.concat(String.valueOf(row));

            ClientXO.result = s;

            // ставим O так как мы клиент
            //temp.setO();
        }
    };

    public void paintButton(int i, int j)
    {
        btnarr[i][j].setO();
    }
}
