package bird;

public class Logic <T extends Bird> {
    T [] array;
    
    Logic(T [] array)
    {
        this.array = array;
    }
    
    void display()
    {
        for(T temp : array)
        {
            temp.move();
        }
    }
    
    int weightController()
    {
        int sum = 0;
        
        for(T temp : array)
        {
            sum += temp.weight;
        }
        return sum;
    }
    
    static void weightCompare(Logic<?> c1, Logic<?> c2)
    {
        System.out.println(c1.weightController() - c2.weightController());
    }
}
