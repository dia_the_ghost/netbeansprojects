package bird;

public class Pengiun extends Bird {
    Pengiun(int weight)
    {
        super(weight);
    }

    @Override
    void move()
    {
        System.out.println("Swim");
    }
}
