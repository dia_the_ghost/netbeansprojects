package bird;

public class Main {

    public static void main(String[] args) {
        Straus[] s = 
        {
            new Straus(55),
            new Straus(65)
        };
        
        Pengiun [] p = 
        {
            new Pengiun(55),
            new Pengiun(65),
            new Pengiun(15)
        };
        
        Logic<Pengiun> l = new Logic<>(p);
        l.display();
        
        Logic<Straus> sl = new Logic<>(s);
        sl.display();
        
        Logic.weightCompare(l, sl);
    }
    
}
