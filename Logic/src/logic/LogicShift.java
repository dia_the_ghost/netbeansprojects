package logic;

public class LogicShift {
    public static void main(String[] args)
    {
        int a = 3;          // 0000 0011
        int b = a << 2;     // 0000 1100        // a * 2^3
        
        // 253 << 5;     253 * 2^5
        
        //System.out.println(b);
        
        byte operand = 0b0000001;
        System.out.println("Число до сдвига: " + operand);
        
        // Логический сдвиг влево
        operand = (byte)(operand << 1);  // 0000 0100
        System.out.println("Число после сдвига влево: " + operand);
        
        // Логический сдвиг вправо
        operand = (byte)(operand >> 1);  // 0000 0010
        System.out.println("Число после сдвига вправо: " + operand);
        
        // беззнаковый сдвиг
        int number = -1 >>> 24;
        System.out.println(number);
    }
}
