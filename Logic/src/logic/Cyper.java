package logic;

// используя операцию ^ мы можем зашифровать сообщение
// в таком случае мы используем ключ для шифрования и ключ для расшифровки
public class Cyper {
    public static void main(String[] args)
    {
        short secretKey = 0b0101;
        char character = 'A';
        System.out.println("Исходный символ:" + character + " Его код " + (byte)character);
        
        // 0 ^ 0 = 0        1 ^ 0 = 1
        // 1 ^ 1 = 0        0 ^ 1 = 1
        
        // зашифровываем символ 65
        character = (char)(character ^ secretKey);  // 0100 0001 ^ 0000 0101
        System.out.println("Измененный символ:" + character + " Его код " + (byte)character);
        
        // Расшифровываем символ 68
        character = (char)(character ^ secretKey);  // 0100 0100 ^ 0000 0101
        System.out.println("Расшифрованный символ:" + character + " Его код " + (byte)character);
        
    }
}
