package logic;

public class Port {
    public static void main(String[] args)
    {
        // 0111 0000 - порт
        // 7654 3210 - нумерация битов управления устройствами

        byte port = 0b1110000;
        byte maskOn = 0b0000010;
        
        System.out.println("port = " + port);
        
        // включаем порт урпавляемый первым байтом
        port = (byte)(port | maskOn);
        System.out.println("port = " + port);
        
        byte maskOff = 0b1111101;
        
        port = (byte)(port & maskOff);
        System.out.println("port = " + port);
        
    }
}
