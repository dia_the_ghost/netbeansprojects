package logic;

public class Logic {

    public static void main(String[] args) {
        byte operand1 = 127;    // 0111 1111
        byte operand2 = 1;      // 0000 0001
        int result;
        
        // 0 & 0 = 0        1 & 0 = 0
        // 0 & 1 = 0        1 & 1 = 1
        
        result = operand1 & operand2;   //0000 0001
        
        System.out.println(operand1 + " AND " + operand2 + " = " + result);
        
        //-----------------------------------------------------------------
        
        operand1 = 2;                   //0000 0010
        operand2 = 1;                   //0000 0001
        result = operand1 | operand2;   //0000 0011
        
        System.out.println(operand1 + " OR " + operand2 + " = " + result);
        
        //------------------------------------------------------------------
        
        // 0 ^ 0 = 0        1 ^ 0 = 1
        // 1 ^ 1 = 0        0 ^ 1 = 1
        
        operand1 = 3;                   //0000 0011
        operand2 = 1;                   //0000 0001
        result = operand1 ^ operand2;   //0000 0010
        
        System.out.println(operand1 + " XOR " + operand2 + " = " + result);
        
        //------------------------------------------------------------------
        
        operand1 = 1;                   //0000 0001
        result = ~operand1;             //1111 1110
        
        System.out.println(operand1 + " NOT = " + result);
        
        // Изменение знака числа
        result = ~operand1;             //1111 1110
        result++;                       //1111 1111
        
        System.out.println(operand1 + " NOT = " + result);
    }
    
}
