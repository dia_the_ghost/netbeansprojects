package lr1ritardo;

public class GravityCalc {

    public static void main(String[] args) {
        double gravity = -9.81;
        double initialVelocity = 0.0;
        double fallingTime = 10.0;
        double initialPosition = 0.0;
        double finalPosition = 0.0;
        
        finalPosition = 0.5 * gravity * Math.pow(fallingTime, 2) + initialVelocity * fallingTime + initialPosition;
        
        System.out.println("Pos in " + fallingTime + " sec: " + finalPosition);
    }
    
}
