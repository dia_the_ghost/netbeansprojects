package lr1ritardo;

public class Factorial {
    public static void main(String[] args)
    {
        int factorial = factorialRecursion(3); // 3! = 6
        //int factorial = factorialCycle(3);
        System.out.println(factorial);
    }
    
    public static int factorialRecursion(int n)
    {
        if(n == 1)
        {
            return 1;
        }
        else
        {
            return n * factorialRecursion(n - 1);
        }

    }
    
    public static int factorialCycle(int n)
    {
        int temp = 1;
        for(int i = 1; i <= n; i++)
        {
            temp *= i;
        }
        return temp;
    }
}
