package lr1ritardo;

import java.util.Random;

public class Matrix {
    static int ROW_LENGTH = 6;
    static int COLUMN_LENGTH = 5;
    static int [][] matrix = new int[ROW_LENGTH][COLUMN_LENGTH];
    
    public static void main(String[] args)
    {
        zapolnit();
        show();
        summaStolbca();
        System.out.println("");
        summaStroki();
    }
    
    public static void zapolnit()
    {
        Random rnd = new Random(System.currentTimeMillis());
        int number;
        for(int row = 0; row < ROW_LENGTH; row++)
        {
            for(int column = 0; column < COLUMN_LENGTH; column++)
            {
                number = 0 + rnd.nextInt(100 - 0 + 1);
                matrix[row][column] = number;
            }
        }
    }
    
    public static void show()
    {
        for(int row = 0; row < ROW_LENGTH; row++)
        {
            for(int column = 0; column < COLUMN_LENGTH; column++)
            {
                System.out.print(matrix[row][column] + "    ");
            }
            System.out.println("");
        }
    }
    
    
    public static void summaStolbca()
    {  
        int m_col = 0;  // номер колонки
        int summa = 0;  // сумма колонки
        int srednyee = 0;
        
        // пока текущая колонка меньше к-ва колонок
        while(m_col < COLUMN_LENGTH)
        {
            // считаем сумму элементов столбца
            for(int i = 0; i < ROW_LENGTH; i++)
            {
                summa += matrix[i][m_col];                              
            } 
            // переходим на следующую колонку
            m_col++;
            
            srednyee = summa / ROW_LENGTH;
            System.out.println("Сумма элементов столбца " + m_col + " равна: " + summa + ", среднее значение:^ " + srednyee);
            
            summa = 0;
            srednyee = 0;
        }
    }
    
    public static void summaStroki()
    {
        int m_row = 0; // номер столбца
        int summa = 0; // сумма столбца
        int srednyee = 0;
        
        while(m_row < ROW_LENGTH)
        {
            for(int i = 0; i< COLUMN_LENGTH; i++)
            {
                summa += matrix[m_row][i];
            }
            // переходим к следующей строке
            m_row++;
            
            srednyee = summa / COLUMN_LENGTH;
            System.out.println("Сумма элементов строки " + m_row + " равна: " + summa + ", среднее значение:^ " + srednyee);
            
            summa = 0;
            srednyee = 0;
        } 
    }
}
