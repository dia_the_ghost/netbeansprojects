package lr1ritardo;

import java.util.Random;

public class Massivi {
    static double [] massiv = new double[200];
    
    public static void zapolnit()
    {
        Random rnd = new Random(System.currentTimeMillis());
        double number;
        
        for(int i = 0; i < massiv.length; i++)
        {
            // рандомим число 
            number = 0.0 + (100.0 - 0.0) * rnd.nextDouble();
            
            massiv[i] = number;
            System.out.println(massiv[i]);
        }
    }
    
    
    public static void main(String[] args)
    {
        zapolnit();
        System.out.println("Srednee:^ " + findSrednee());
        System.out.println("Menshee:^ " + findMenshee());
        System.out.println("Bolshee:^ " + findBolshee());
        posortirovat();
        show();
    }
    
    public static double findMenshee()
    {
        double menshee = 100.0; 
        for(int i = 0; i < massiv.length; i++)
        {
            if(menshee > massiv[i])
            {
                menshee = massiv[i];
            }
        }
        return menshee;
    }
    
    public static double findBolshee()
    {
        double bolshee = 0.0;
        
        for(int i = 0; i < massiv.length; i++)
        {
            if(bolshee < massiv[i])
            {
                bolshee = massiv[i];
            }
        }
        return bolshee;
    }
    
    public static double findSrednee()
    {
        double srednee;
        double summa = 0;
        
        for(int i = 0; i < massiv.length; i++)
        {
            summa += summa+massiv[i];
        }
        srednee = summa / 200;
        return srednee;
    }
    
    public static void posortirovat()
    {
        for (int i = massiv.length - 1; i > 0; i--)
        {
            for (int j = 0; j < i; j++)
            {
                if (massiv[j] > massiv[j + 1]) 
                {
                    double temp = massiv[j];
                    massiv[j] = massiv[j + 1];
                    massiv[j + 1] = temp;
                }
            }
        }
    }
    
    public static void show()
    {
        for(int i = 0; i < massiv.length; i++)
        {
            System.out.println(massiv[i]);
        }
    }
}
