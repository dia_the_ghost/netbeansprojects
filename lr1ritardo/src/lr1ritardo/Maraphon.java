package lr1ritardo;

public class Maraphon {
    static String [] names = {"Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", 
        "Alex", "Emma", "John", "James", "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate"};
    static int [] times = {341, 273, 278, 329, 445, 402, 338, 275, 243, 334, 412, 393, 299, 343, 317, 265};
    
    public static void main(String [] args)
    {
        show();
    }
    
    public static void show()
    {
        for(int i = 0; i < names.length; i++)
        {
            System.out.println(names[i] + ", "+ times[i] + "");
        }
    }
}
