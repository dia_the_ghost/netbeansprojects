package hwrit2;

public class Book implements Comparable<Book> { // ограничение в дженерике
    private String author;
    private String name;
    private int timesPublished;

    Book(String author, String name, int timesPublished) {
        this.author = author;
        this.name = name;
        this.timesPublished = timesPublished;
    }


    public int getTimesPublished()
    {
        return this.timesPublished;
    }
    public String getAuthor()
    {
        return this.author;
    }
    
    public String getName()
    {
        return this.name;
    }

    //от меньшего к большему
    // возвращает 0, если объекты равны,
    // -1 если первый объект меньше второго,
    // 1 если первый больше.
    @Override
    public int compareTo(Book book) {
        return this.getTimesPublished() > book.getTimesPublished() ? 1 : this.getTimesPublished() == book.getTimesPublished() ? 0 : -1;
    }
    
    @Override
    public String toString()
    {
        String s = "Автор: " + this.getAuthor() + "; Название: " + this.getName() + "; Дата публикации: " + this.getTimesPublished();
        return s;
    }
}