package hwrit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Homework2 {
    private static Set<Map.Entry<String, String>> entrySet;
    
    public static void main(String[] args) {
        Map <String, String> awards = new HashMap <>();

        List<Book> books = new ArrayList<>();
        
        
        awards.put("Shakespeare","Too much drama");
        awards.put("Swift","Survival guide");
        awards.put("Austen","Did not read");
        awards.put("Dumas","Sweet revenge");
        
        books.add(new Book("Dumas", "The Count of Monte Cristo", 1245));
        books.add(new Book("Shakespeare", "Romeo and Juliet", 4500));
        books.add(new Book("Austen", "Pride and Prejudice", 1000));
        books.add(new Book("Swift", "Gulliver's Travels", 1000));
        books.add(new Book("Tolstoy", "War and Peace", 1000));
        
        entrySet = awards.entrySet();
        //showSet();
        
        // Сначала книги сравниваются по timesPublished, от меньшего к большему.
        
        // возвращает 0, если объекты равны,
        // отрицательное число если первый объект меньше второго,
        // положительное число  если первый больше.
        
        print(books);
        System.out.println("Sort by publishing");
        

        final int SIZE = books.size() - 1;
        // от большего к меньшему
        /*
        for (int i = 1; i < SIZE - 1; ++i)
        {
            for (int j = 0; j < SIZE - i; j++)
            {
                int t = books.get(j).compareTo(books.get(j+1));
                if (t < 0) Collections.swap(books, j, j+1);
                
                else if(t == 0)
                {
                    // свапаем по алфавиту
                }
            }
        }
*/
        // от меньшего к большему
        for(int i = 0; i < SIZE; i++)
        {
            for(int j = 0; j < SIZE - i; j++)
            {
                int t = books.get(j).compareTo(books.get(j+1));
                if(t > 0) Collections.swap(books, j, j+1);
                
                // СВАП ПО НАЗВАНИЯМ КНИГИ (не по автору)
                if(t == 0)
                {
                    // меньше 0 если аргумент больше строки
                    // больше 0 если аргемент меньше строки
                    int result = books.get(j).getName().compareTo(books.get(j+1).getName());
                    if(result > 0) 
                    {
                        Collections.swap(books, j, j+1);
                    }
                }
            }
        }
        
        System.out.println("After sorting ");
        //print(books);
       
        printBooks(books);  

    }
    
    public static void printBooks(List<Book> books)
    {
        System.out.println("--------------Books---------------");
        for (Book temp : books)
        {
            System.out.println("The book "  + temp.getName() + " by author " + temp.getAuthor() 
                    + " which sold " + temp.getTimesPublished() + " copies, received: " + findAward(temp.getAuthor()) + " award ");
        }
        System.out.println(" ");
    }
    public static void print(List<Book> books)
    {
        for (Book temp : books)
        {
            System.out.println(temp.toString());
        }
    }
    private static String findAward(String author) 
    {
        for (Map.Entry<String,String> temp : entrySet)
        {
            if (temp.getKey().equals(author))
            {
                return temp.getValue();
            }
        }
        return "no";
    }
    
    private static void showSet()
    {
        for (Map.Entry<String, String> temp : entrySet)
        {
            System.out.println(temp.getKey() + " : " + temp.getValue());
        }
    }
    
}
