package hwrit3;

public class Pekarnya {
    int counter = 0;
    final int MAXMAFFINS = 10;
    
    // синхронизированный метод для производителей
    synchronized int bake() 
    {
        if(counter < MAXMAFFINS) 
        {
            counter++;
            //System.out.println ("в пекарне есть " + counter + " маффинов");
            return 1;
        }
        return 0;
    }
    
     // метод для покупателей
    synchronized int buy() 
    {
        if(counter > 0) //если хоть один маффин есть
        {
            counter--;
            //System.out.println ("в пекарне есть " + counter + " маффинов");
            return 1;
        }
        return 0;
    }
}
