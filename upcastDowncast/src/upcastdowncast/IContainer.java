package upcastdowncast;

public interface IContainer<T> {
    T getFigure();
    
    void setFigure(T figure);
}
