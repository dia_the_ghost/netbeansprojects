package upcastdowncast;

public class Main {
    
    public static void main(String[] args) {
        Circle circle = new Circle();
        
        IContainer<Circle> container = new Container<>(circle);
        
        System.out.println(container.getFigure().toString());
    }
    
}
