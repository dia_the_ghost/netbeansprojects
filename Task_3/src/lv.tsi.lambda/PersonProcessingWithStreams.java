package lv.tsi.lambda;

import java.util.List;
import java.util.ArrayList;

public class PersonProcessingWithStreams {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Max", 28));
        personList.add(new Person("Peter", 22));
        personList.add(new Person("Anna", 28));
        personList.add(new Person("Lilianna", 16));
        personList.add(new Person("Pamela", 13));
        personList.add(new Person("David", 22));

        PersonEvalutor personEvalutor = new PersonEvalutor(personList);
        System.out.println("Самому старшему хуесосу: " + personEvalutor.getAverageAge().getAge());
        System.out.println("Средний возраст хуесосов: " + personEvalutor.getMediumAge());
        personEvalutor.findAndPrint();
        System.out.println("Старше 18 лет: ");
        personEvalutor.olderThan();
        System.out.println();
        System.out.println("Группируем: ");
        personEvalutor.groupAndPrint();
    }
}
