package lv.tsi.lambda;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.List;
import java.util.Arrays;

public class ListProcessingWithStreams {
    public static void main(String[] args) {
        // "a1", "c3","a2", "a3","b3", "b2", "c1", "c2", "b1"
        Stream<String> li = Stream.of("a1", "c3", "a2", "a3", "b3", "b2", "c1", "c2", "b1");
        //li.forEach(System.out::println);

        List<String> list = Arrays.asList("a1", "c3", "a2", "a3", "b3", "b2", "c1", "c2", "b1");

        System.out.println(list.stream().findFirst());

        list.stream().forEach((String value) -> System.out.println(value.toUpperCase()));
        System.out.println("--------------------------");

        list.stream().filter(s -> s.startsWith("b")).forEach(System.out::println);
        System.out.println("--------------------------");

        list.stream().filter(s -> s.startsWith("c")).sorted().forEach((String value) -> System.out.println(value.toUpperCase()));



    }
}
