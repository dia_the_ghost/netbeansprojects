package lv.tsi.lambda;

import java.util.List;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonEvalutor {
    public List<Person> persons;
    private int counter = 0;

    PersonEvalutor(List<Person> p)
    {
        this.persons = p;
    }

    // находит переданный в функцию элемент который начинается на букву P
    public void findAndPrint()
    {
        persons.stream().filter(s -> s.getName().startsWith("P"))
                .forEach((Person p) -> System.out.println(p.toString()));
    }

    // печатает хуесосов, старше 18 лет
    public void olderThan()
    {
        persons.stream().filter(s -> s.getAge() > 18).forEach(System.out::println);

        // System.out::println печатает объект у которого вызван метод toString()
        // если toString не переопределить то он напечатает кракозябры
    }

    // группирует по возрасту
    public void groupAndPrint()
    {
        Map<String, Integer> temp = persons.stream()
                .collect(Collectors.toMap(p -> p.getName(), t -> t.getAge()));

        temp.forEach((o1,o2) -> System.out.println(o1 + " " + o2));
    }

    // ищет самый большой возраст в списке и печатает его
    public Person getAverageAge()
    {
        Comparator <Person> comparator = (p1, p2) -> Integer.compare(p1.getAge(), p2.getAge());
        Person maxAge = persons.stream().max(comparator).get();
        //Person minAge = persons.stream().min(comparator).get();

        return maxAge;
    }

    // ищет средний возраст
    public  int getMediumAge()
    {
        // пробегаемся по каждому объекту
        persons.stream().forEach((Person p) -> inc(p.getAge()));

        return counter / persons.size();
    }

    public void inc(int age)
    {
        counter = counter + age;
    }
}
