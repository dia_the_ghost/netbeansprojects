package enumerations;

public enum Company {
    ITVDN(1000), GOOGLE(100), SKYNET(10);
    int value;
    
    //ctor can be only private
    Company(int value)
    {
        this.value = value;
    }
    
    public int getValue()
    {
        return value;
    }
}
