package enums;

public class Main {
    public static void main(String[] args)
    {
        Day today = Day.MONDAY;
        
        //System.out.println("Today is " + today);
        
        switch(today)
        {
            case SATURDAY:
                System.out.println("Today is saturday");
                break;
                
            case SUNDAY:
                System.out.println("Today is sunday");
                break;
                
            case MONDAY:
                System.out.println("Today is monday");
                break;
                
            case TUESDAY:
                System.out.println("Today is tuesday");
                break;
                
            case THURSDAY:
                System.out.println("Today is thursday");
                break;
                
            case FRIDAY:
                System.out.println("Today is friday");
                break;
        }
        
        if(today == Day.MONDAY)
        {
            System.out.println("MONDAY!!!!!!!!");
        }
    }
}
