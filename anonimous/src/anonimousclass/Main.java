package anonimousclass;

public class Main {
    public static void main(String[] args)
    {
        // объявление анонимного внутреннего класса
        InterfaceM instance = new InterfaceM()
        {
            int field = 9;
            
            @Override
            public void method() {
                field = 100;
            }

            @Override
            public int getValue() {
                return field;
            }
        };
        // объявление анонимного внутреннего класса
        InterfaceM instance2 = new InterfaceM()
        {
            int field = -1;

            @Override
            public void method() {
                field = instance.getValue();
            }

            @Override
            public int getValue() {
                return field;
            }
            
        };
        instance.method();
        instance2.method();
        System.out.println(instance.getValue());
        System.out.println(instance2.getValue());
    }
}
