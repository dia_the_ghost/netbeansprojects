package abstract_methods;

public enum Company {
    ITVDN(1000){
        // можно переопределять методы для отдельных экземпляров
        @Override
        public int getValue()
        {
            return 10 + this.value;
        }

        // все абстрактные методы нужно реализовать
        @Override
        public String getCurrency() {
            return "dollars";
        }
    },
    GOOGLE(1000){
        // фбстрактные методы обязательно должны быть переопределены в экземплярах перечислямых типов
        @Override
        public String getCurrency()
        {
            return "cents";
        }
        
    },
    SKYNET(10){
        
        @Override
        public String toString()
        {
            return "Company" + super.toString();
        }
        @Override
        public String getCurrency()
        {
            return "euros";
        }
    };
    
    int value = 10;
    
    // ctor
    Company(int value)
    {
        this.value = value;
    }
    
    // methods
    public int getValue()
    {
        return value;
    }
    public abstract String getCurrency();
    
}
