package abstract_methods;

public class Main {
    public static void main(String [] args)
    {
        Company myComp = Company.ITVDN;
        System.out.println(myComp);
        
        // вызов методов
        int salary = myComp.getValue();
        String currency = myComp.getCurrency();
        System.out.println(" My salary is " + salary + " " + currency + " in a month");
    }
}
