package anonimousclandInterface;

public class ConcreteClass implements Interface{
    protected int protectedField = 10;
    
    @Override
    public void method() {
        System.out.println("ConcreteClass: method() ");
    }

    @Override
    public int getValue() {
        return protectedField;
    }
    
}
