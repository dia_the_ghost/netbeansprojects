package anonimousclandInterface;

public class Main {
    public static void main(String[] args)
    {
        // объявление анонимного внутреннего класса
        ConcreteClass instance = new ConcreteClass()
        {
            @Override
            public void method()
            {
                System.out.println("instance: method() ");
            }
        };
        
        // объявление анонимного внутреннего класса через DownCast
        Interface instance2 = new ConcreteClass()
        {
            int protectedField = 3;
            
            @Override
            public void method()
            {
                System.out.println("instance2: method() ");
            }
            
            @Override
            public int getValue()
            {
                return this.protectedField + super.getValue(); // 3 + 10
            }
        };
        
        instance.method();
        instance2.method();
        
        System.out.println("instance: publicField " + instance.getValue());
        System.out.println("instance2: publicField " + instance2.getValue());
        
        System.out.println(instance2.getClass().toString()); // имя анонимного класса        
    }
}
