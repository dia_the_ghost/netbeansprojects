package anonimous;

public class Mobile extends Computer {
    
    // фабрика
    Phone createPhone()
    {
        // возвращаем внутренний анонимный класс
        return new Phone()
        {
            @Override
            void process()
            {
                super.process();
                System.out.println("Mobile ");
            }
        };
    }
    
    void process()
    {
        super.process();
        System.out.println("Mobile ");
    }
    
}
