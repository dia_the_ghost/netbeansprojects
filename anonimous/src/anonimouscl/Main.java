package anonimouscl;

public class Main {
    public static void main(String[] args)
    {
        // локальные переменные доступны в блоке анонимного класса
        int local = 100;
        
        // объявление анонимного внутреннего класса
        Interface1 instance;
        instance = new Interface1() {
            // поля
            public double d = 1.3;
            protected String str = "sssss";
            private int num = 10;
                 
            // методы
            @Override
            public void method()
            {
                num = 1234;
                num = local;
                System.out.println("instance: " + d + str + num);
            }
            
            public double getPublicField()
            {
                return d;
            }
        };
        instance.method();
        
        //
        //System.out.println(instance.d);  // публичная переменная анонимного класса недоступна
        //System.out.println(instance.getPublicField); // публичный геттер недоступен
    }
}
