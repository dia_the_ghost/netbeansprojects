package dota;

import java.util.Arrays;

public class Run {
    
    public static void main(String[] args) {
        Hero[] massiv = {
            new Hero("Windranger", "core", 26),
            new Hero("Crystal Maiden", "support", 21),
            new Hero("Pudge", "support", 23),
            new Hero("Bloodseeker", "core", 12),
        };
        
        Arrays.sort(massiv, Hero.HeroNameComparator);
        
        for(Hero temp : massiv)
        {
            System.out.println(temp.getName());
        }
        
        Arrays.sort(massiv, Hero.HeroNumberComparator);
        
        for (Hero temp : massiv)
        {
            System.out.println(temp.getNumber());
        }
        
        Arrays.sort(massiv, Hero.HeroRoleComparator);
        
        for(Hero temp : massiv)
        {
            System.out.println(temp.getRole());
        }
    }
    
}
