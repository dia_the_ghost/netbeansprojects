package dota;

import java.util.Comparator;

public class Hero {

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    private String name;
    private String role;
    private int number;
    
    Hero(String name, String role, int number)
    {
        super();
        this.name = name;
        this.role = role;
        this.number = number;
    }
    
    public static Comparator<Hero> HeroNameComparator = new Comparator<Hero>()
    {
        @Override
        public int compare(Hero t, Hero t1) {
             String heroName = t.getName().toUpperCase();
             String heroName1 = t1.getName().toUpperCase();
             
             // ascending order
             return heroName.compareTo(heroName1);
             
             // descending order
             //return heroName1.compareTo(heroName);
        }
        
    };
    
    public static Comparator<Hero> HeroNumberComparator = new Comparator<Hero>()
    {
        @Override
        public int compare(Hero t, Hero t1) {
            int heroNumber = t.getNumber();
            int heroNumber1 = t1.getNumber();
            
            return heroNumber - heroNumber1;
        }
        
    };
    
    public static Comparator<Hero> HeroRoleComparator = (Hero t, Hero t1) -> {
        String heroRole = t.getRole().toUpperCase();
        String heroRole1 = t1.getRole().toUpperCase();
        
        return heroRole.compareTo(heroRole1);
    };
}
