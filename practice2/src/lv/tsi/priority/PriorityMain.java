package lv.tsi.priority;

public class PriorityMain {
    public static void main(String[] args) {
        PriorityThread min = new PriorityThread("min");
        PriorityThread norm = new PriorityThread("norm");
        PriorityThread max = new PriorityThread("max");

        min.setPriority(Thread.MIN_PRIORITY);
        norm.setPriority(Thread.NORM_PRIORITY);
        max.setPriority(Thread.MAX_PRIORITY);

        min.start();
        norm.start();
        max.start();
    }
}
