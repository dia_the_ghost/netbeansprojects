package lv.tsi.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/*
  Интерфейс BlockingQueue является очередью (Queue),
  т.е. его элементы хранятся в порядке «первый пришел, первый вышел» (FIFO – first in, first out).
  Элементы, вставленные в коллекцию в определенном порядке, будут извлечены из нее в том же самом порядке.
  Также интерфейс гарантирует, что любая попытка извлечь элемент из пустой очереди заблокирует вызывающий поток до тех пор,
  пока в коллекции не появится элемент, который можно извлечь. Аналогично, любая попытка вставить элемент в заполненную очередь
  заблокирует вызывающий поток, пока в коллекции не освободится место для нового элемента.

 BlockingQueue изящно решает проблему передачи элементов,
 собранных одним потоком, для обработки в другой поток без явных хлопот о проблемах синхронизации.
 */
public class BlockingMain {
    public static void main(String[] args) {
        final BlockingQueue<Integer> bq = new LinkedBlockingQueue<Integer>();
        AddingThread at = new AddingThread(bq);
        TakingThread tt = new TakingThread(bq);

        at.run();
        tt.run();
    }
}
