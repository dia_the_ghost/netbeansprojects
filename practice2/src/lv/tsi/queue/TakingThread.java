package lv.tsi.queue;

import java.util.concurrent.BlockingQueue;

public class TakingThread extends Thread{
    BlockingQueue<Integer> queue;
    TakingThread(BlockingQueue queue)
    {
        super();
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 3; i != 0; --i)
        {
            queue.remove(i);
            System.out.println("Removed: " + i);
        }
        System.out.println(queue);
    }
}
