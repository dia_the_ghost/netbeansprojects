package lv.tsi.queue;

import java.util.concurrent.BlockingQueue;

public class AddingThread extends Thread {
    private BlockingQueue<Integer> queue;
    AddingThread(BlockingQueue queue)
    {
        super();
        this.queue = queue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; ++i)
        {
            queue.add(i);
            System.out.println("Adding: " + i);
        }
        System.out.println(queue);
    }
}
