package lv.tsi.producer_consummer;

public class Goods {
    private int amount = 0;

    public int getAmount() {
        return amount;
    }

    public synchronized void produce(int amount)
    {
        if(amount > 0)
        {
            try
            {
                // освобождает монитор и переводит вызывающий поток в состояние ожидания до тех пор,
                // пока другой поток не вызовет метод notify()
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            amount++;
            System.out.println("Added");
            // продолжает работу потока, у которого ранее был вызван метод wait()
            notify();
            //return amount;
        }
    }

    public synchronized int consume()
    {
        if (amount < 1)
        {
            try
            {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            amount--;
            System.out.println("Consumed");
            notify();
        }
        return amount;
    }
}
