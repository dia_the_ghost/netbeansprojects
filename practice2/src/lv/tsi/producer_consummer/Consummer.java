package lv.tsi.producer_consummer;

public class Consummer extends Thread {
    private Goods goods;
    Consummer(Goods goods)
    {
        super();
        this.goods = goods;
    }

    @Override
    public void run() {
        goods.consume();
        System.out.println(goods.getAmount());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
