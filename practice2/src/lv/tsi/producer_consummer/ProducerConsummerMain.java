package lv.tsi.producer_consummer;

public class ProducerConsummerMain {
    public static void main(String[] args) {
        Goods goods = new Goods();
        Producer p = new Producer(goods);
        Consummer c = new Consummer(goods);

        p.start();
        c.start();
    }
}
