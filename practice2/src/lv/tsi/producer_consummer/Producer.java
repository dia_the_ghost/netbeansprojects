package lv.tsi.producer_consummer;

public class Producer extends Thread{
    private Goods goods;
    Producer(Goods goods)
    {
        super();
        this.goods = goods;
    }

    @Override
    public void run() {
        goods.produce(1);
        System.out.println(goods.getAmount() + " добавляем 1 товар ");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
