package lv.tsi.sync;

public class SyncMain {
    public static void main(String[] args) {
        final TaskManager taskManager = new TaskManager();

        TaskThread taskThread1 = new TaskThread("taskThread1", 101, taskManager);
        TaskThread taskThread2 = new TaskThread("taskThread2", 121, taskManager);

        try {
            taskThread1.start();
            Thread.sleep(2000);
            taskThread2.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
