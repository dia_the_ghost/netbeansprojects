package lv.tsi.sync;

public class TaskThread extends Thread {
    private TaskManager taskManager;
    private int taskId;

    TaskThread(String name, int taskId, TaskManager tm)
    {
        // в ctor нельзя задать id потока
        super(name);
        this.taskId = taskId;
        this.taskManager = tm;
    }

    @Override
    public void run() {
        taskManager.performTask(taskId);
    }
}
