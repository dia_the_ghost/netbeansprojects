package lv.tsi.sync;

public class TaskManager {
    synchronized void performTask(int taskId)
    {
        try {
            System.out.println(Thread.currentThread().getName() + " Id: " + Thread.currentThread().getId() + " - " + taskId);
            Thread.sleep(400);
            System.out.println(Thread.currentThread().getName() + " Id: " + Thread.currentThread().getId()  + " - " + taskId);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
