import java.util.Collection;
import java.util.Map;

public class User {
    String name;
    int id;
    GamesPlayed gamesPlayed;
    Map<String, Integer> map;
    Collection collection;

    User(String name, int id, GamesPlayed gamesPlayed, Map map, Collection collection)
    {
        this.name = name;
        this.id = id;
        this.gamesPlayed = gamesPlayed;
        this.map = map;
        this.collection = collection;
    }
}
