import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        GamesPlayed gamesPlayed = new GamesPlayed(new Dota2(3200, 106), new CsGo(3500, 84));

        Map<String, Integer> map = new LinkedHashMap<>();
        map.put("USD", 26);
        map.put("UA", 1);

        Collection collection = new ArrayList();
        collection.add("string");
        collection.add(100);
        collection.add(true);

        User user = new User("Vasyan228", 15662341, gamesPlayed, map, collection);
        int [] massiv = {12, 25, 228};
        Steam steam = new Steam("127.0.0.1", 8080, user, massiv);

        GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
        Gson gson = builder.create();

        //System.out.println("GSON" + ":" + gson.toJson(barsik));
        writeJson(gson.toJson(steam));


        //String jsonText = "{'name':'Мурзик','age':8}";
        //Cat murzik = gson.fromJson(jsonText, Cat.class);
        //System.out.println(murzik);
    }


    public static void writeJson(String str)
    {
        try(FileWriter writer = new FileWriter("C:\\Users\\Dia\\IdeaProjects\\Pojo\\src\\main\\resources\\json.txt", false))
        {
            writer.write(str);
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
