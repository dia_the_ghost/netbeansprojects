public class Steam {
    public String port;
    public int socket;
    public User user;
    public int [] massiv;

    Steam(String port, int socket, User user, int [] massiv)
    {
        this.port = port;
        this.socket = socket;
        this.user = user;
        this.massiv = massiv;
    }
}
