package ex_02;

public class Main{
    public static void main(String[] args) {
        try{
            throw new Exception ("My exception");
        }
        catch(Exception e)
        {
            System.out.println("Обработка исключения");
            System.out.println(e.getMessage());
        }
        // для закрытия потоков
        finally{
            System.out.println("Выводится всегда");
        }
    }
    
}
