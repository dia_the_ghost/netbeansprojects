package ex_01;

public class Main{
    public static void main(String[] args) {
        Exception ex = new Exception("My exception");
        
        try{
            throw ex;
        }
        catch(Exception e)
        {
            System.out.println("Обработка исключения");
            System.out.println(e.getMessage());
        }
    }
    
}
