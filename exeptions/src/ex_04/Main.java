package ex_04;

import java.io.FileInputStream;

public class Main{
    public static void main(String[] args) {
        try
        {
            throw new UserException();
        }
        catch(UserException userException)
        {
            System.out.println("Обработка исключения ");
            userException.method();
            
            try
            {
                FileInputStream fs = new FileInputStream("C:\\NonExistenFile.log");
            }
            catch(Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }
    }
}
