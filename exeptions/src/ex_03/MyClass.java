package ex_03;

import java.lang.Exception;

public class MyClass {
    public void myMethod() throws Exception
    {
        Exception exception = new Exception("My exception");
        throw exception;
    }
}
