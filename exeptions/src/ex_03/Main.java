package ex_03;

public class Main{
    public static void main(String[] args) {
        try{
            MyClass instance = new MyClass();
            instance.myMethod();
        }
        catch(Exception e)
        {
            System.out.println("Message: " + e.getMessage());
            System.out.println("Stack trace: ");
            //e.printStackTrace();
        }
        finally
        {
            
        }
    }
    
}
