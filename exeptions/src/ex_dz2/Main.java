package ex_dz2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {
    
    // метод проверки на валидность
    public static boolean isValid(String date, String dateFormat) 
    {
        if(date == null)
        {
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        
        try
        {
            // если не валидное то выбросит исключение
            Date d = sdf.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public static void main(String[] args) throws IOException, ParseException{       
        
        Worker [] massiv = new Worker[4];
        //ArrayList <Worker> massiv = new ArrayList<>();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        for(int i = 0; i < massiv.length; ++i)
        {
            System.out.println("Enter Name, job and year");
            
            //Scanner sc = new Scanner(System.in);
            //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
            String Name = br.readLine();
            String Job = br.readLine();
            String Year = br.readLine();
            
            isValid(Year, "yyyy");
            
            massiv[i] = new Worker(Name, Job, Year);
            //massiv.add(w);
        }
       
        
        for(Worker temp : massiv)
        {
            temp.show();
        }
        
        // Вводим значение по условию
        System.out.println("Input value:");
        int value = Integer.parseInt(br.readLine());
        // Нынешний год чтоб узнать стаж работника
        int yearNow = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < massiv.length; i++) {
            String year = massiv[i].getYear();
            // Узнаем стаж
            int staj = yearNow - Integer.parseInt(year);
            // По выполнению условия вывод на экран
            if (staj > value) {
                massiv[i].show();
            }
        }
    }
}
