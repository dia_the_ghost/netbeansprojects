package ex_dz2;

public class Worker {
    private String name;
    private String job;
    private String year;
    
    Worker(String name, String job, String year)
    {
        this.name = name;
        this.job = job;
        this.year = year;
    }
    public void setName(String n)
    {
        this.name = n;
    }
    public void setJob(String j)
    {
        this.job = j;
    }
    public void setYear(String y)
    {
        this.year = y;
    }
    public String getYear()
    {
        return this.year;
    }
    
    public void show()
    {
        System.out.println("Name: " + this.name + ", Job: " + this.job + ", Year: " + this.year);
    }
}
