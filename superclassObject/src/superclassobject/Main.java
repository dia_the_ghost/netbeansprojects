package superclassobject;

public class Main {

    public static void main(String[] args) {
        String s1 = "hello";
        String s2 = "hell";
        
        if(!s1.equals(s2))
        {
            System.out.println(s1.hashCode());
            System.out.println(s2.hashCode());
            System.out.println(s1.getClass());
        }
        
        Car car = new Car();
        System.out.println(car.toString());
    }
    
}

class Car extends Object{
    public int weight = 200;
    public int height = 100;
    Car()
    {
        super();
    }
    
    @Override
    public String toString()
    {
        String s;
        s = Integer.toString(weight);
        return s;
    }
}