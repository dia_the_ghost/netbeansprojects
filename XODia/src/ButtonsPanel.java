import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class ButtonsPanel extends JPanel {
    ICallback callback = null;
    GridLayout gl;

    // тут храним на кнопки
    ArrayList<MyButton> li = new ArrayList();
    MyButton[][] btnarr;

    // стартовые значения для создания и удаления поля
    int nRows = 3;
    int nCols = 3;

    // ctor
    public ButtonsPanel()
    {
        gl = new GridLayout(nRows, nCols);
        createField(nRows, nCols);
    }

    public void setCallback(ICallback callback){
        this.callback = callback;
    }

    public void createField(int rows, int columns)
    {
        gl.setRows(rows);
        gl.setColumns(columns);


        this.setLayout(gl);
        this.btnarr = new MyButton[rows][columns];

        // добавление кнопок в цикле
        for(int i = 0; i < rows; ++i)
        {
            for(int j = 0; j < columns; ++j)
            {
                addButton(mouseListener, i, j);
            }
        }

        this.updateUI();
        this.repaint();
    }

    private void addButton(MouseListener l, int i, int j)
    {
        MyButton b = new MyButton(i, j);
        b.addMouseListener(l);
        b.setPreferredSize(new Dimension(60, 60));
        // добавляем в список
        li.add(b);
        btnarr[i][j] = b;

        this.add(b);
    }

    MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent me) {
            MyButton temp = (MyButton)me.getSource();

            int row = 0;
            int column = 0;
            // ищем координаты кнопки
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    if(temp.equals(btnarr[i][j])) {
                        row = i;
                        column = j;
                        if (callback != null)
                            callback.onButtonClick(i, j);
                    }
                }
            }

            // отправляем координаты кликнутой кнопки на сервер
            // через функцию Send()
            String s = "";
            s.concat(String.valueOf(column));
            s.concat(":");
            s.concat(String.valueOf(row));

            //ClientXO.result = s;
        }
    };

    public void paintButtonO(int i, int j)
    {
        btnarr[i][j].setO();
    }
    public void paintButtonX(int i, int j)
    {
        btnarr[i][j].setX();
    }

}
