import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientLogics implements ICallback {
    Gui gui;
    BufferedReader readerToClientFromServer;
    public PrintWriter fromClientToServer;
    String str = "";
    public static String result = "";

    boolean isMyTurn = false;
    Socket socket = null;

    public ClientLogics()
    {
        createGui();

        try{
            socket = new Socket("127.0.0.1", 8080);
            readerToClientFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            //TODO add writer
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // поток для чтения
        new Thread(new Reciever()).start();
    }

    private void createGui()
    {
        gui = new Gui();

        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setLocationRelativeTo(null); // центрировать окно
        gui.setTitle("Client XO");
        gui.setVisible(true);

        gui.panel.setCallback(this);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                ClientLogics client = new ClientLogics();
                //server.run();
            }
        });
    }
    @Override
    public void onButtonClick(int x, int y){
        if (isMyTurn) {
            gui.panel.paintButtonO(x, y);
            result = String.valueOf(x) + ":" + String.valueOf(y);
            Send();
            isMyTurn = false;
        }
    }

    public void Send ()
    {
        System.out.println("Send: " + result);
        // get socket
        try{
            // тут он ему дает
            // а должен давать не тут
            fromClientToServer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fromClientToServer.println(result);
    }

    private class Reciever implements Runnable {
        @Override
        public void run() {
            // принимаем то что дал сервер и парсим
            while (!socket.isClosed())
            {
                try{
                    while ((str = readerToClientFromServer.readLine()) != null)
                    {
                        System.out.println(str);
                        String [] params = str.split(":");
                        int row = Integer.parseInt(params[0]);
                        int column = Integer.parseInt(params[1]);

                        gui.panel.paintButtonX(row, column);
                        //if(panel != null) panel.paintButton(row, column);

                        isMyTurn = true;
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

