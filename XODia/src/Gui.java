import javax.swing.*;
import java.awt.*;

public class Gui extends JFrame{
    public ButtonsPanel panel;

    // ctor
    // будет создаваться объект класса гуи в логике в Main
    public Gui()
    {
        GridBagLayout mainLayout = new GridBagLayout();
        this.setLayout(mainLayout);

        panel = new ButtonsPanel();

        this.add(panel);
        pack();
    }

}
