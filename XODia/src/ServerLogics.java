import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ServerLogics implements Runnable, ICallback {

    ServerSocket ss = null;
    int [][] board;
    private Thread serverThread;
    boolean isMyTurn = true;
    SocketProcessor processor;
    BlockingQueue<SocketProcessor> q = new LinkedBlockingQueue<>();

     Gui gui;

    // ctor
    public ServerLogics(int port)
    {
        createGui();

        board = new int[3][3];

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {

                board[i][j] = 0;
            }
        }

        // стартуем сервер
        try
        {
            //serverIp = InetAddress.getLocalHost();
            ss = new ServerSocket(port);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createGui()
    {
        gui = new Gui();

        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setLocationRelativeTo(null); // центрировать окно
        gui.setTitle("Server XO");
        gui.setVisible(true);

        gui.panel.setCallback(this);
    }

    @Override
    public void onButtonClick(int x, int y) {
        if (isMyTurn) {
            gui.panel.paintButtonX(x, y);
            processor.Send(x, y);
        }
    }

    // ожидает новое подключение // воно пробує отримувати підключення
    private Socket getNewConnection()
    {
        Socket s = null;
        try {
            s = ss.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void run() {
        serverThread = Thread.currentThread();
        Socket d = null;
        while (true) {
            d = getNewConnection();
            if (d != null)
                break;
        }

        System.out.println("Connection was taken!");
        if (!serverThread.isInterrupted()) {
            if (d != null) {
                try {
                    processor = new SocketProcessor(d);
                    final Thread thread = new Thread(processor);
                    thread.setDaemon(true);
                    thread.start();
                    q.offer(processor);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                ServerLogics server = new ServerLogics(8080);
                server.run();
            }
        });
    }

    private class SocketProcessor implements Runnable {
        Socket s;
        BufferedReader br;
        PrintWriter pw;

        //ctor
        SocketProcessor(Socket socketParam) throws IOException
        {
            s = socketParam;
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            pw = new PrintWriter(s.getOutputStream(), true);
        }

        public void Send(int x, int y) {
            String outputLine = String.valueOf(x);
            outputLine = outputLine.concat(":");
            outputLine = outputLine.concat(String.valueOf(y));

            isMyTurn = false;

            pw.println(outputLine);
        }

        @Override
        public void run() {
            System.out.println("RUNNING!");
            while (!s.isClosed())
            {
                // считываем то что нам кинул клиент и парсим на 2 инта
                String line;
                try{
                    line = br.readLine();
                    System.out.println("Answer from client: " + line);
                    String [] coords = line.split(":");
                    int row = Integer.parseInt(coords[0]);
                    int column = Integer.parseInt(coords[1]);

                    gui.panel.paintButtonO(row, column);
                    isMyTurn = true;

                    // TODO проверяем есть ли такой ход и если нету то добавляем
                    // TODO кидаем ход клиентам
                    /// !> Зупиняти поток
                    //String outputLine = String.valueOf(currentRow);
                    //outputLine = outputLine.concat(":");
                    //outputLine = outputLine.concat(String.valueOf(currentCol));


                    //pw.println(outputLine);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
