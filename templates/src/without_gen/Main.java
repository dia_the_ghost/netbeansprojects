package without_gen;

public class Main {
    public static void main(String[] args)
    {
        WithoutGen w1 = new WithoutGen("Test");
        WithoutGen w2 = new WithoutGen(10);
        WithoutGen w3 = new WithoutGen(new Cat("Marsel"));

        String s = (String)w1.getObj();
        
        w1.display();
        w2.display();
        w3.display();
        
        System.out.println(s);
    }
}
