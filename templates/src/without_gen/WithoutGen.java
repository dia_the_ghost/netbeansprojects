package without_gen;

public class WithoutGen {
    Object obj;
    
    Object getObj()
    {
        return this.obj;
    }
    
    //ctor
    WithoutGen(Object obj)
    {
        this.obj = obj;
    }
    
    void display()
    {
        System.out.println(obj);
    }
}
