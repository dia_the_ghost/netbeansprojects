package templates;

public class Cat {
    String name;
    
    // ctor
    Cat(String name)
    {
        this.name = name;
    }
    
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
