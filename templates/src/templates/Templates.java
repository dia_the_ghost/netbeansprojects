package templates;

import java.util.ArrayList;
import java.util.Iterator;

public class Templates {

    public static void main(String[] args) {
        WithGen<String> w1 = new WithGen<>("Test");
        WithGen<Integer> w2 = new WithGen<>(10);
        WithGen<Cat> w3 = new WithGen<>(new Cat("Marsel"));
        
        ArrayList<WithGen> li = new ArrayList<>();
        li.add(w1);
        li.add(w2);
        li.add(w3);
        
        // списком
        Iterator<WithGen> it = li.iterator();
        while(it.hasNext())
        {
            WithGen<Object> temp = it.next();
            temp.display();
        }
        
        // массивом
        WithGen [] arr = {
            new WithGen<>("Test"),
            new WithGen<>(10),
            new WithGen<>(new Cat("Marsel"))
        };
        
        for(WithGen temp : arr)
        {
            temp.display();
        }

        
    }
    
}
