package templates;

public class WithGen<T> {
    T obj;
    
    WithGen(T obj)
    {
        this.obj = obj;
    }
    
    void display()
    {
        System.out.println(obj);
    }
}
