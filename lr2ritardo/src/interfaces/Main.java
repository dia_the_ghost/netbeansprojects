package interfaces;

public class Main {
    public static void main(String [] args)
    {
        /* Интерфейсы
        Невозможно создать экземпляр интерфейса
        Интерфейся не имели реализации в java до 8 версии
        Интерфейс может содержать только абстрактные члены(даже если слово abstract не написано)
        Члены интерфейса ВСЕГДА public abstract но им нельзя написать модификатор доступа
        (потому что они блять итак всегда public)
        Интерфейс может implements другой интерфейс
        Интерфейс не может содержать конструктор или вложенный тип
        
        верни стримы
        
        */
        //IAnimal cat = new Cat();
        //IAnimal dog = new Dog();
        //IAnimal fox = new Fox();
        
        Cat cat = new Cat("Murzik228");
        Dog dog = new Dog("Pesik322");
        Fox fox = new Fox();
        
        // хочется запихать 3 животных в массив и в форе у всех элементов массива вызывать sound() и printName()
        // но нельзя потому что мы унаследовались от 2х интерфейсов а не от 1го
        
        cat.sound();
        cat.printName();
        dog.sound();
        dog.printName();
        fox.sound();
        fox.svoiMethod();
        
        // это рофл никому не нужны твои стримы
    }
}
