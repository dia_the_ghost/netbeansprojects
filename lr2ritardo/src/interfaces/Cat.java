
package interfaces;

public class Cat implements IAnimal, IDomestic{
    private String name;
    
    // ctor
    Cat(String name)
    {
        this.name = name;
    }

    @Override
    public void sound() {
        System.out.println("Meow");
    }

    @Override
    public void printName() {
        System.out.println("My name is: " + name);
    }
    
}
