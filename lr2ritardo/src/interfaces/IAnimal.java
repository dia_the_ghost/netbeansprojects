package interfaces;

// рих интерфейсы всегда именуются с I (IMyInterface, ICat, IDog) чтобы легко по именам отличать от классов
public interface IAnimal {
    void sound();
}
