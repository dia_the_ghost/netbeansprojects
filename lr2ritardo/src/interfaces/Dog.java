package interfaces;

public class Dog implements IAnimal, IDomestic {
    private String name;
    
    // ctor
    Dog(String name)
    {
        this.name = name;
    }

    @Override
    public void sound() {
        System.out.println("Bark");
    }
    
    @Override
    public void printName() {
        System.out.println("My name is: " + name);
    }
    
}
