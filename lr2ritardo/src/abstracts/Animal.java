package abstracts;

public abstract class Animal {
    String name;
    
    //Java разрешит описать конструкторы в классе Animal
    // но не разрешит ими воспользоваться (потому что запрещено создавать объекты абстрактного класса). 
    Animal()
    {
        
    }
    
    // Абстрактным называется метод, который не имеет реализации в данном классе. 
    // если унаследовать класс и в потомках переопределить метод, задав там его описание,
    // то для объектов классов потомков метод можно будет вызывать
    abstract void sound();
    abstract String name();
    
    void nonAbstracrName()
    {
        System.out.println("Mt name is:^ " + name);
    }
}
