package abstracts;

public class Cat extends Animal {
    // поле name подтягивается из Animal    
    // ctor
    Cat(String name)
    {
        this.name = name;
    }
    
    @Override
    public void sound() {
        System.out.println("Meow");
    }

    @Override
    public String name() {
        return this.name;
    }

}
