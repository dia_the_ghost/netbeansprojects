package abstracts;

public class Dog extends Animal {
    //ctor
    Dog(String name)
    {
        this.name = name;
    }
    
    @Override
    public void sound() {
        System.out.println("Bark");
    }

    @Override
    public String name() {
        return this.name;
    }
}
