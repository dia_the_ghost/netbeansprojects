package methodsmainoverload;

import java.lang.Math;

// вывод двоичного числа, переданного в десятичном формате
public class BinaryDigit {
    
    public static void converter(int n)
    {
        int temp;
        
        temp = n % 2;
        
        if(n >= 2)
            converter(n / 2);
        
        System.out.print(temp);
    }
    
    public static void toDecimal(String s)
    {
        int temp = 0;
        // 10100
        // 43210
        
        // s.length() = 5;
        
        for(int i = s.length(); i > 0; i--)
        {
            if(s.charAt(i - 1) == '1')
            {
                temp += (int)(Math.pow(2, i + 1));
            }           
        }
       
        
        System.out.print(temp);
    }
    
    public static void main(String[] args)
    {
        //int n = 20;
        
        //converter(n);
        //System.out.println();
        
        String s = "10100";
        
        toDecimal(s);
        System.out.println();
    }
}
