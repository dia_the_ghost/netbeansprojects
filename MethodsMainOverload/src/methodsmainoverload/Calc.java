package methodsmainoverload;

import java.util.Scanner;

public class Calc {
    
    public static int addition(int a, int b)
    {
        int temp = a + b;
        
        return temp;
    }
    
    public static int subtraction(int a, int b)
    {
        int temp = a - b;
        
        return temp;
    }
    
    public static int multiplication(int a, int b)
    {
        int temp = a * b;
        
        return temp;
    }
    
    public static int division(int a, int b)
    {
        int temp = a / b;
        
        return temp;
    }
    
    public static void main(String[] args)
    {
        int a, b;
        int c;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter: ");
        
        a = sc.nextInt();
        c = sc.next().charAt(0);
        b = sc.nextInt();
        
        switch(c)
        {
            case 43:
                System.out.println(a  + "+" + b + " = " + addition(a, b));
                break;
                
            case 45:
                System.out.println(a  + "-" + b + " = " + subtraction(a, b));
                break;
                
            case 42:
                System.out.println(a  + "*" + b + " = " + multiplication(a, b));
                break;
                
            case 47: 
                System.out.println(a  + "/" + b + " = " + division(a, b));
                break;
                
            default:
                System.out.println("Default");
                break;
        }
        
        
    }
}
