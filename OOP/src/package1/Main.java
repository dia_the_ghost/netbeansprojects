
package package1;

import package2.ClassTwo;
import package2.Parent;

public class Main extends Parent {
    public static void main(String[] args) {
        ClassOne el = new ClassOne();
        
        //System.out.println(el.name1); // private
        System.out.println(el.name2);   // package
        System.out.println(el.name3);   // protected
        System.out.println(el.name4);   // public
        
        ClassTwo elem = new ClassTwo();
        
        //System.out.println(elem.name1);
        //System.out.println(elem.name2);
        //System.out.println(elem.name3);
        System.out.println(elem.name4);
        
        Main p = new Main();
        
        //System.out.println(p.name1);   // private
        //System.out.println(p.name2);   // package
        System.out.println(p.name3);   // protected
        System.out.println(p.name4);     // public
    }
}
