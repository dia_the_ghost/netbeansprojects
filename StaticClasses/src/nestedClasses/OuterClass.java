package nestedClasses;

public class OuterClass {
    
    public int x = 9;
    
    // внутренний класс
    // вложенный внутренний класс не может содержать в себе статических методов или статических полей
     public class InnerClass
     {
         public int a = 10;
         
         public void show()
         {
             System.out.println("Метод внутреннего класса");
         }
     }
     public static class InnerStaticClass{
         public static int s = 12;
         public static void showS()
         {
             System.out.println("12 = " + s);
         }
     }
     
     public void outerMethod()
     {
         class InnerClassA
         {
             public void innerMethod()
             {
                 System.out.println("Метод внутреннего класса");
             }
         }
         InnerClassA i = new InnerClassA();
         i.innerMethod();
     }
}
