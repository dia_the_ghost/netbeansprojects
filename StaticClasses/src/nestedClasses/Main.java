package nestedClasses;

public class Main {
    public static void main(String[] args) {
        OuterClass m = new OuterClass();
        OuterClass.InnerClass inner = new OuterClass().new InnerClass();
        
        System.out.println(m.x);
        System.out.println(inner.a);
        
        inner.show();
        
        m.outerMethod();
        
        // вызов статического метода из статического вложенного класса
        OuterClass.InnerStaticClass.showS();
        OuterClass.InnerStaticClass d = new OuterClass.InnerStaticClass();
    }
}
