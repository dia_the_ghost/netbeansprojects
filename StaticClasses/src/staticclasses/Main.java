package staticclasses;

public class Main {

    public static void main(String[] args) {
        NonStaticClass.staticMethod();
        NonStaticClass m = new NonStaticClass(1);
        NonStaticClass n = new NonStaticClass(2);
        
        m.method();
        n.method();
        
        NonStaticClass.field = 3;
        
        m.method();
        n.method();
        
        NonStaticClass.staticMethod();
    }
    
}
